using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public static Manager Singleton;

    public float TimeRate = 1.5f;

    public int Rate = 10;

    public int Love = 100;
    public int Funny = 100;
    public int Hungry = 100;

    public Pet Pet;

    public Pet[] Status_Pet;

    int Elapsed_Time;

    int Current_Status = 0;

    public Slider Slider_Hungry; 
    public Slider Slider_Love; 
    public Slider Slider_Funny;

    public Button Button_Hungry;
    public Button Button_Funny;
    public Button Button_Love;

    public Animator Animator_Pet;

    private void Awake()
    {
        Animator_Pet = FindAnyObjectByType<Animator>();

        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        Button_Hungry.onClick.AddListener(Feed_Pet);
        SetUp();
        InvokeRepeating("Update_Life_Bars", 0, TimeRate);


        Button_Love.onClick.AddListener(Love_Pet);
        SetUp();
        InvokeRepeating("Update_Life_Bars", 0, TimeRate);
    

        Button_Funny.onClick.AddListener(Play_with_Pet);
        SetUp();
        InvokeRepeating("Update_Life_Bars", 0, TimeRate);
    }

    public void Animator_Hungry() 
    {       
        Animator_Pet.SetBool("Hungry", true);
        StartCoroutine(Timer_Animations(Animator_Pet, "Hungry"));       
    }


    public void Animator_Funny()
    {
        Animator_Pet.SetBool("Play", true);
        StartCoroutine(Timer_Animations(Animator_Pet, "Play"));
    }


    public void Animator_Love()
    {
        Animator_Pet.SetBool("Love", true);
        StartCoroutine(Timer_Animations(Animator_Pet, "Love"));
    }


    IEnumerator Timer_Animations(Animator animations, string nameAnimation)
    {
        yield return new WaitForSeconds(1);
        animations.SetBool(nameAnimation, false);
    }

    void Play_with_Pet()
    {
        if (Funny < 100)
        {
            Funny = 100;
        }
    }

    void Love_Pet()
    {
        if (Love < 100)
        {
            Love = 100;
        }
    }

    void Feed_Pet()
    {
        if(Hungry < 100)
        {
            Hungry = 100;
        }
    }

    void SetUp()
    {
        Slider_Hungry.value = Hungry;
        Slider_Love.value = Love;
        Slider_Funny.value = Funny;          
    }

    int Time;

    void Update_Life_Bars()
    {
        if (Time > Rate)
        {
            Time = 0;

            // Randomly select one of the bars
            int selectedBar = Random.Range(0, 3); // 0 for Funny, 1 for Love, 2 for Hungry

            // Decreases the randomly selected bar
            DecreaseLife(selectedBar);

            // Update all bars
            Slider_Funny.value = Funny;
            Slider_Love.value = Love;
            Slider_Hungry.value = Hungry;
        }

        Time++;
        Elapsed_Time++;
    }

    void DecreaseLife(int selectedBar)
    {
        int decreaseAmount = Random.Range(1, 6); // Random value between 1 and 5

        switch (selectedBar)
        {
            case 0: // Funny
                Funny -= decreaseAmount;
                if (Funny <= 0)
                {
                    Die();
                }
                break;
            case 1: // Love
                Love -= decreaseAmount;
                if (Love <= 0)
                {
                    Die();
                }
                break;
            case 2: // Hungry
                Hungry -= decreaseAmount;
                if (Hungry <= 0)
                {
                    Die();
                }
                break;
            default:
                break;
        }
    }

    void Die()
    {
        /*Destroy(Pet.gameObject);
        Pet = Instantiate(Status_Pet[0], Vector3.zero, Quaternion.identity) as Pet;
        Current_Status = -1;*/

        Destroy(Pet.gameObject);

        if (Status_Pet[0] != null)
        {
            Pet = Instantiate(Status_Pet[0], Vector3.zero, Quaternion.identity) as Pet;
        }
        else
        {
            Debug.LogError("El prefab de Status_Pet[0] no est� asignado.");
        }

        Current_Status = -1;
    }
}


/* 
  
    Quer�a hacer el proyecto en 3D como lo dice la prueba, pero no tengo tanta experiencia en 3D, en el SENA hice algunas cosas 3D, pero normalmente no era de los que se encargaban de la parte de programaci�n (No es mi fuerte), s� que eso va a afectar demasiado en el resultado.
    
    Aqu� les comparto un link a una carpeta de mi Drive, me hace ilusi�n que la puedan ver :D

    Muchas gracias
  
  
    https://drive.google.com/drive/folders/1n08xNX6T2GJegQ9UCEfa3dw9Xgfz-yde?usp=sharing

 */

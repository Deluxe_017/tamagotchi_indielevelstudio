using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Change_Escene : MonoBehaviour
{
    public void Change_Esc(string name)
    {
        SceneManager.LoadScene(name);
    }
}

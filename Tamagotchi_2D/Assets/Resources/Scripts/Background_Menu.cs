using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background_Menu : MonoBehaviour
{
    [SerializeField]
    RawImage scrollableImage;
    [SerializeField]
    float scrollSpeed;
    Rect Rect;
    [SerializeField]
    Vector2 scrollDirection;

    private void Start()
    {
        Rect = scrollableImage.uvRect;
    }

    private void Update()
    {
        Rect.x += scrollDirection.x * scrollSpeed * Time.deltaTime;
        Rect.y += scrollDirection.y * scrollSpeed * Time.deltaTime;
        
        scrollableImage.uvRect = Rect;
    }
}
